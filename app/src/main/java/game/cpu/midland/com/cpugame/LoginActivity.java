package game.cpu.midland.com.cpugame;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private EditText etPwd1, etPwd2, etPwd3, etPwd4;


    private static final String PASSWORD = "0360";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etPwd1 = (EditText) findViewById(R.id.etPwd1);
        etPwd2 = (EditText) findViewById(R.id.etPwd2);
        etPwd3 = (EditText) findViewById(R.id.etPwd3);
        etPwd4 = (EditText) findViewById(R.id.etPwd4);


        etPwd1.addTextChangedListener(new PasswordTextWatcher(etPwd1, etPwd2));
        etPwd2.addTextChangedListener(new PasswordTextWatcher(etPwd2, etPwd3));
        etPwd3.addTextChangedListener(new PasswordTextWatcher(etPwd3, etPwd4));
        etPwd4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                Log.i(TAG,"beforeTextChanged");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                Log.i(TAG,"onTextChanged");
            }

            @Override
            public void afterTextChanged(Editable editable) {
//                Log.i(TAG,"afterTextChanged");
                if (editable.length() > 0){
                    attemptLogin();
                }
            }
        });

        etPwd2.setOnKeyListener(new PasswordEditTextOnKeyListener(etPwd2, etPwd1));
        etPwd3.setOnKeyListener(new PasswordEditTextOnKeyListener(etPwd3, etPwd2));
        etPwd4.setOnKeyListener(new PasswordEditTextOnKeyListener(etPwd4, etPwd3));


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        Button btnPlayTestVideo = (Button) findViewById(R.id.btnPlayTestVideo);
        btnPlayTestVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startVRVideoActivity();
            }
        });



//        PackageManager packageManager = getPackageManager();
//        boolean gyroExists = packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
//        if (!gyroExists) {
//            DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    dialogInterface.dismiss();
//                }
//            };
//            showAlertDialog("", getString(R.string.no_gyrosensor), onClickListener);
//        }


//        processLogin();
    }

    private void attemptLogin() {
//        Log.i(TAG,"attemptLogin");
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        };


        String inputPassword = etPwd1.getText().toString() + etPwd2.getText().toString() + etPwd3.getText().toString() + etPwd4.getText().toString();
        if (inputPassword.equals(PASSWORD)) {
            processLogin();
        } else {
            showAlertDialog("", getString(R.string.error_incorrect_password), onClickListener);
            etPwd1.setText("");
            etPwd2.setText("");
            etPwd3.setText("");
            etPwd4.setText("");
            etPwd1.requestFocus();
        }
    }

    private void processLogin() {
//        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
//            startActivity(new Intent(this, ChooseVideoActivity.class), ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
//        } else{
//
//        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        startActivity(new Intent(this, ChooseVideoActivity.class));
        finish();
    }

    private void startVRVideoActivity(){
        startActivity(new Intent(this, VrVideoActivity.class).putExtra("isTest", true));
    }

    private void showAlertDialog(String title, String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title).setMessage(message);
        alertDialogBuilder.setPositiveButton(getString(R.string.ok), onClickListener);
        alertDialogBuilder.setCancelable(false);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private class PasswordEditTextOnKeyListener implements View.OnKeyListener{
        EditText editTextMe, editTextPrev;

        public PasswordEditTextOnKeyListener(EditText editTextMe, EditText editTextPrev){
            this.editTextMe = editTextMe;
            this.editTextPrev = editTextPrev;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (editTextMe.length() > 0) {

                    } else {
                        editTextPrev.requestFocus();
                        editTextPrev.selectAll();
                    }
                }
            }
            return false;
        }
    }

    private class PasswordTextWatcher implements TextWatcher{
        EditText editTextMe, editTextNext;

        public PasswordTextWatcher(EditText editTextMe, EditText editTextNext){
            this.editTextMe = editTextMe;
            this.editTextNext = editTextNext;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() > 0) {
                editTextMe.clearFocus();
                editTextNext.requestFocus();
            }
        }
    }

}

