package game.cpu.midland.com.cpugame;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class ChooseVideoActivity extends AppCompatActivity {

    ImageView ivThumbnail1, ivThumbnail2, ivThumbnail3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_video);

        ivThumbnail1 = (ImageView) findViewById(R.id.ivThumbnail1);
        ivThumbnail2 = (ImageView) findViewById(R.id.ivThumbnail2);
        ivThumbnail3 = (ImageView) findViewById(R.id.ivThumbnail3);

        ivThumbnail1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVRVideoActivity(1);
            }
        });

        ivThumbnail2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVRVideoActivity(2);
            }
        });

        ivThumbnail3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVRVideoActivity(3);
            }
        });

    }

    private void startVRVideoActivity(int videoIndex){
        startActivity(new Intent(this, VrVideoActivity.class).putExtra("videoIndex", videoIndex));
    }

}
